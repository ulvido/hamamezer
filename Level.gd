extends Node

onready var gem = preload("res://Gem.tscn")
onready var gem_container = $Container_Gem
onready var timer = $Timer

onready var skor_label = $HUD/Label_Skor
onready var level_label = $HUD/Label_Level
onready var timer_label = $HUD/Label_Timer
onready var gameover_label = $HUD/Label_Gameover

var screensize
var score = 0
var level = 1

signal gameover

func _ready():
	gameover_label.visible = false
	randomize()
	screensize = get_viewport().size
	set_score()
	set_level()
	spawn_gems(10)
	timer.start()

func _process(delta):
	timer_label.set_text(str(int(timer.get_time_left())))
	
	if gem_container.get_child_count() == 0:
		level += 1
		set_level()
		spawn_gems(10 * level)
		timer.set_wait_time(timer.get_wait_time() - 1)
		timer.start()

func spawn_gems(num):
	for i in range(num):
		var g = gem.instance()
		var scale_factor = rand_range(0.8,1)
		g.set_scale(Vector2(scale_factor, scale_factor))
		gem_container.add_child(g)
		g.connect("gem_grabbed",self,"_on_gem_grabbed")
		g.set_position(Vector2(rand_range(40,screensize.x-40),rand_range(40,screensize.y-40)))

func _on_gem_grabbed():
	score += 10
	set_score()

func set_score():
	skor_label.text = "SKOR:" + str(score)

func set_level():
	level_label.text = "LEVEL:" + str(level)

func _on_Timer_timeout():
	emit_signal("gameover")
	gameover_label.visible = true
	$AudioStreamPlayer2D.play()
	$Player.set_process(false)