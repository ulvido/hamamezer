extends Area2D

var screensize
var extents
var pos
export var speed = 400
var vel = Vector2()
var anim = "idle"

func _ready():
	screensize = get_viewport_rect().size
	#extents = $Sprite.get_texture().get_size() / 2
	extents = $CollisionShape2D.get_shape().get_extents()
	set_position(screensize / 2)


func _process(delta):
	var input = Vector2(0,0)
	input.x = int(Input.is_action_pressed("ui_right")) - int(Input.is_action_pressed("ui_left"))
	input.y = int(Input.is_action_pressed("ui_down")) - int(Input.is_action_pressed("ui_up"))
	vel = input.normalized() * speed
	
	# yönünü dönsün
	if input.x > 0 || input.y > 0:
		$Sprite.set_animation("run")
		$Sprite.scale.x = -1
	elif input.x < 0 || input.y < 0:
		$Sprite.set_animation("run")
		$Sprite.scale.x = 1
	else:
		$Sprite.set_animation("idle")
	
	# limit position to screen size. karakter ekran dışına çıkmasın
	pos = get_position() + vel * delta
	pos.x = clamp(pos.x,extents.x,screensize.x-extents.x)
	pos.y = clamp(pos.y,extents.y,screensize.y-extents.y)
	set_position(pos)
