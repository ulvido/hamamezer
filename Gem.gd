extends Area2D

onready var effect = $Effect
onready var sprite  = $Sprite

signal gem_grabbed

func _ready():
	# büyüsün
	effect.interpolate_property(sprite, "scale", 
	sprite.get_scale(), Vector2(2.0,1.2), 
	0.1, Tween.TRANS_QUAD, Tween.EASE_OUT, 0)
	# saydamlaşsın
	effect.interpolate_property(sprite,"modulate",
	Color(1,1,1,1),Color(1,1,1,0.8),
	0.5, Tween.TRANS_QUAD,Tween.EASE_OUT, 0)

func _on_Gem_area_entered(area):
	if area.get_name() == "Player":
		emit_signal("gem_grabbed")
		# bi kere alınca hızlıca girip çıkarsa bi daha sayıyor. onu engellemek için.
		var owners = get_shape_owners()
		shape_owner_clear_shapes(owners[0])
		# efekti başlat
		effect.start()

func _on_Effect_tween_completed(object, key):
	$AudioStreamPlayer2D.play()

func _on_AudioStreamPlayer2D_finished():
	queue_free()
