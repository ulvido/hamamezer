extends Node

onready var level = preload("res://Level.tscn").instance()
onready var menu = preload("res://Menu.tscn").instance()

func _ready():
	menu.connect("basla", self, "_on_basla")
	add_child(menu)

func _on_basla():
	add_child(level)

# loop audio
func _on_AudioStreamPlayer2D_finished():
	$AudioStreamPlayer2D.play()
